"use client"
import { useState } from 'react';
import React from 'react'
import { Alert, AlertTitle } from '@mui/material';

function Header({ title = 'Default title' }) {
  return <h1>{title ? title : 'Default title'}</h1>;
}

const Alerta = () => {
  return (
    <Alert severity="error">
      <AlertTitle>Error</AlertTitle>
      No puedes Agregar valores vacios a las lista — <strong>check it out!</strong>
    </Alert>
  )
}

export default function HomePage() {
  const names = ['Ada Lovelace', 'Grace Hopper', 'Margaret Hamilton'];
  const [likes, setLikes] = useState(0);
  const [input, setInput] = useState('');
  const [title, setTitle] = useState('');
  const [arr, setArr] = useState<string[]>([]);
  const [showAlert, setShowAlert] = useState(false); // State to control alert visibility

  function handleClick() {
    setLikes(likes + 1);
  }
  console.log(input)

  return (
      <div>
        <center>
          <Header title="Develop. Preview. Ship. 🚀" />
          <ul>
            {names.map((name) => (
              <li key={name}>{name}</li>
            ))}
          </ul>
          <ul>
            <input
              style={{ color: 'blue' }}
              onChange={v => setInput(v.target.value)}
            />
          </ul>
          <ul>
            <button
              onClick={() => {
                if (!input) {
                  setShowAlert(true); // Show the alert
                } else {
                  setShowAlert(false); // Hide the alert
                  setTitle(String(input))
                  if (arr.length <= 0) {
                    setArr([input])
                  } else {
                    setArr([...arr, input])
                  }
                }
                setInput('')
              }}
            >AGREGAR</button>
          </ul>
          <ul>
            {title ? title : "No Title"}
          </ul>
          <table style={{ border: '1px solid black', }}>
            <thead>
              <tr>
                <th scope="col">Key</th>
                <th scope="col">Text</th>
              </tr>
            </thead>
            <tbody>
              {arr.map((text, key) => (
                <React.Fragment key={key}>
                  <tr>
                    <td>{`${Number(key + 1)}`}</td>
                    <td>{`${String(text)} `}</td>
                  </tr>
                </React.Fragment>
              ))}
            </tbody>
          </table>
          <button onClick={handleClick}>Like ({likes})</button>
          {showAlert && <Alerta />}
        </center>
      </div>
  );
}